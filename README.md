# Micro Proxy

#### Requisites
* [Install Git](https://git-scm.com/downloads) 
* [Install OpenJDK 8](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)
* [Install Maven 3](https://maven.apache.org/download.cgi)

#### How to run and test the application

* Open your terminal
* execute the next command to clone the app:
``` shell
    git clone https://gitlab.com/vickyfoukou/microproxy.git 
``` 
    
* navigate into the project directory : 
``` shell
    cd microproxy
```
* run this command :
``` shell
    mvn spring-boot:run
```
    
###### NB :  This command should populate the database with 2 default customers where their IDs are 1 and 2
* open your favorite web browser and access to the UI 
    - http://127.0.0.1:8000  

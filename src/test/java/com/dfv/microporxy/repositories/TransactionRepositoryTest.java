package com.dfv.microporxy.repositories;

import com.dfv.microporxy.entities.Account;
import com.dfv.microporxy.entities.Customer;
import com.dfv.microporxy.entities.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class TransactionRepositoryTest {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    CustomerRepository customerRepository;


    @Test
    public void findAllTransactionsByAccountReturnsAnNoEmptyList(){
        Customer customer = Customer.builder()
                .name("Djoulako")
                .surname("Vicky")
                .build();
        customerRepository.save(customer);

        Account account = Account.builder()
                .balance(100)
                .customer(customer)
                .build();
        accountRepository.save(account);

        Transaction transaction = Transaction.builder()
                .account(account)
                .amount(account.getBalance())
                .build();
        transactionRepository.save(transaction);

        List<Transaction> transactionList = transactionRepository.findAllByAccount(account);

        assertThat(transactionList).isNotNull();
        assertThat(transactionList.size()).isEqualTo(1);
    }
}

package com.dfv.microporxy.repositories;


import com.dfv.microporxy.entities.Account;
import com.dfv.microporxy.entities.Customer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;


import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AccountRepositoryTest {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    CustomerRepository customerRepository;


    @Test
    public void findByCustomReturnsOptionAccountTest(){
        Customer customer = Customer.builder()
                .name("Djoulako")
                .surname("Vicky")
                .build();
        customerRepository.save(customer);

        Account account = Account.builder()
                .balance(0)
                .customer(customer)
                .build();
        accountRepository.save(account);

        Optional<Account> optionalAccount = accountRepository.findByCustomer(customer);
        assertThat(optionalAccount.isPresent()).isEqualTo(true);
    }
}

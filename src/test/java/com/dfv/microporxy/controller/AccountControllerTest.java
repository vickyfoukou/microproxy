package com.dfv.microporxy.controller;

import com.dfv.microporxy.dto.OpenAccountDTO;
import com.dfv.microporxy.dto.UserAccountDTO;
import com.dfv.microporxy.services.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AccountController.class)
@ExtendWith(MockitoExtension.class)
public class AccountControllerTest {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;

    private OpenAccountDTO openAccountDTO;


    @BeforeEach
    public void setup(){
        openAccountDTO = OpenAccountDTO.builder().customerId(1).initialCredit(0).build();
    }



    @Test
    public void openAccountTest() throws Exception {
       doNothing().when(accountService).open_account(openAccountDTO);

       String response =  mvc.perform(
                post("/accounts/open")
               .content(mapper.writeValueAsString(openAccountDTO))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
               )
                .andExpect(status().isOk())
               .andReturn()
               .getResponse().getContentAsString();

       assertThat(response).contains("New current account successfully created");
    }

    @Test
    public void openAccountWithNegativeValuesTest() throws Exception {
        openAccountDTO.setCustomerId(-1);
        openAccountDTO.setInitialCredit(-1);
        doNothing().when(accountService).open_account(openAccountDTO);

        String response =  mvc.perform(
                post("/accounts/open")
                        .content(mapper.writeValueAsString(openAccountDTO))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        )
                .andExpect(status().isBadRequest())
                 .andReturn()
                 .getResponse().getContentAsString();

        assertThat(response).contains("customerId");
        assertThat(response).contains("initialCredit");
    }

    @Test
    public void getUserInfoTest() throws Exception {
        long customerId = 1;
        UserAccountDTO userAccountDTO = UserAccountDTO.builder()
                .balance(100)
                .name("Djoulako")
                .surname("Vicky")
                .transactions(new ArrayList<>())
                .build();

        when(accountService.getUserInfo(ArgumentMatchers.anyLong())).thenReturn(userAccountDTO);

        String response =  mvc.perform(get(String.format("/accounts/get_details/%s", customerId))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).isNotNull();
    }
}

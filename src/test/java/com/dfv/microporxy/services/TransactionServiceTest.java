package com.dfv.microporxy.services;


import com.dfv.microporxy.entities.Account;
import com.dfv.microporxy.entities.Customer;
import com.dfv.microporxy.entities.Transaction;
import com.dfv.microporxy.repositories.TransactionRepository;
import com.dfv.microporxy.serviceimpl.TransactionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionServiceImpl transactionService;

    private Account account;

    private Transaction transaction;

    @BeforeEach
    public void setup(){
        account = Account.builder()
                .id(1)
                .balance(100)
                .customer(Customer.builder().id(1).name("Djoulako").surname("Vicky").build())
                .transactionsList(new ArrayList<>())
                .build();

        transaction = Transaction.builder()
                .amount(100)
                .account(account)
                .build();
    }


    @Test
    public void saveTransactionTest(){
        when(transactionRepository.save(any(Transaction.class))).thenReturn(transaction);
        Transaction savedTransaction =  transactionService.save(transaction);

        assertThat(savedTransaction).isNotNull();
    }

    @Test
    public void findAllByAccount(){
        when(transactionRepository.findAllByAccount(any(Account.class)))
                .thenReturn(Collections.singletonList(transaction));

        List<Transaction> transactionList =  transactionService.findAllByAccount(account);

        assertThat(transactionList).isNotNull();
        assertThat(transactionList.size()).isEqualTo(1);
    }
}

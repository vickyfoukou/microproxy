package com.dfv.microporxy.services;


import com.dfv.microporxy.dto.OpenAccountDTO;
import com.dfv.microporxy.dto.UserAccountDTO;
import com.dfv.microporxy.entities.Account;
import com.dfv.microporxy.entities.Customer;
import com.dfv.microporxy.entities.Transaction;
import com.dfv.microporxy.exceptions.ResourceNotfoundException;
import com.dfv.microporxy.repositories.AccountRepository;
import com.dfv.microporxy.repositories.CustomerRepository;
import com.dfv.microporxy.serviceimpl.AccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private TransactionService transactionService;

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private AccountServiceImpl accountService;

    private Customer customer;

    private OpenAccountDTO openAccountDTO;


    @BeforeEach
    public void setup(){
        customer = Customer.builder().name("Djoulako").surname("Vicky").build();
        openAccountDTO = OpenAccountDTO.builder().customerId(1L).initialCredit(0).build();
    }


    @Test
    public void getCustomerReturnsNotNull() throws ResourceNotfoundException {
        when(customerRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.ofNullable(customer));
        Customer existingCustomer = accountService.getCustomer(1L);
        assertThat(existingCustomer).isNotNull();
    }


    @Test
    public void openAccountWithInitialCreditSetToZero() throws ResourceNotfoundException {
        when(customerRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.ofNullable(customer));
        Customer existingCustomer = accountService.getCustomer(openAccountDTO.getCustomerId());

        Account account = Account.builder()
                .customer(existingCustomer)
                .balance(openAccountDTO.getInitialCredit())
                .build();
        when(accountRepository.save(Mockito.any(Account.class))).thenReturn(account);
        assertAll(() -> accountService.open_account(openAccountDTO));
    }


    @Test
    public void openAccountWithInitialCreditGreaterThanZero() throws ResourceNotfoundException {
        openAccountDTO.setInitialCredit(100);

        when(customerRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.ofNullable(customer));
        Customer existingCustomer = accountService.getCustomer(openAccountDTO.getCustomerId());

        Account account = Account.builder()
                .customer(existingCustomer)
                .balance(openAccountDTO.getInitialCredit())
                .build();
        when(accountRepository.save(Mockito.any(Account.class))).thenReturn(account);

        Transaction transaction = Transaction.builder()
                .amount(openAccountDTO.getInitialCredit())
                .account(account)
                .build();
        when(transactionService.save(Mockito.any(Transaction.class))).thenReturn(transaction);
        assertAll(() -> accountService.open_account(openAccountDTO));
    }


    @Test
    public void getUserInfoReturnsAccountDTO() throws ResourceNotfoundException {
        long customerId = 1;
        float amount = 100;
        customer.setId(customerId);

        when(customerRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.ofNullable(customer));
        Account account = Account.builder()
                .balance(amount)
                .customer(customer)
                .id(1)
                .build();
        when(accountRepository.findByCustomer(Mockito.any(Customer.class))).thenReturn(Optional.ofNullable(account));

        Transaction transaction = Transaction.builder()
                .id(1)
                .account(account)
                .amount(amount)
                .uuid(UUID.randomUUID().toString())
                .transactionTime(LocalDateTime.now())
                .build();
        when(transactionService.findAllByAccount(Mockito.any(Account.class)))
                .thenReturn(Collections.singletonList(transaction));

        UserAccountDTO userAccountDTO = accountService.getUserInfo(customerId);

        assertThat(userAccountDTO).isNotNull();
        assertThat(userAccountDTO.getName()).isEqualTo(customer.getName());
        assertThat(userAccountDTO.getSurname()).isEqualTo(customer.getSurname());
        assertThat(userAccountDTO.getTransactions().size()).isEqualTo(1);

        assert account != null;
        assertThat(userAccountDTO.getBalance()).isEqualTo(account.getBalance());

    }

}

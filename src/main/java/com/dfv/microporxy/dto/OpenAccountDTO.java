package com.dfv.microporxy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpenAccountDTO {
    @Positive
    private long customerId;

    @PositiveOrZero
    private float initialCredit;
}

package com.dfv.microporxy.dto;

import com.dfv.microporxy.entities.Transaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDTO {
    private String id;
    private float amount;
    private LocalDateTime transactionTime;

    public TransactionDTO(Transaction transaction){
        id = transaction.getUuid();
        amount = transaction.getAmount();
        transactionTime = transaction.getTransactionTime();
    }
}

package com.dfv.microporxy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserAccountDTO {
    private String name;
    private String surname;
    private float balance;
    private List<TransactionDTO> transactions;
}

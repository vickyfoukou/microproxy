package com.dfv.microporxy.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "transactions")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false, unique = true, length = 254)
    private String uuid;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(columnDefinition = "decimal(11,2) default 0")
    private float amount;

    @Column
    private LocalDateTime transactionTime;

    @PrePersist
    private void prePersistUuidAndTransactionTime(){
        this.setUuid(UUID.randomUUID().toString());
        this.setTransactionTime(LocalDateTime.now());
    }
}

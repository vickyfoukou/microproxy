package com.dfv.microporxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroporxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroporxyApplication.class, args);
	}

}

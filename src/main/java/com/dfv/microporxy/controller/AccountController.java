package com.dfv.microporxy.controller;

import com.dfv.microporxy.dto.OpenAccountDTO;
import com.dfv.microporxy.dto.UserAccountDTO;
import com.dfv.microporxy.exceptions.ApiResponse;
import com.dfv.microporxy.exceptions.ResourceNotfoundException;
import com.dfv.microporxy.services.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/accounts")
@Slf4j
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/open", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse> openAccount(@RequestBody @Valid OpenAccountDTO openAccountDTO) throws ResourceNotfoundException {
        accountService.open_account(openAccountDTO);
        String message = "New current account successfully created";
        log.info(message);
        return new ResponseEntity<>(ApiResponse.builder()
                .message(message)
                .data("")
                .status(true)
                .build(), HttpStatus.OK);
    }

    @GetMapping( value = "/get_details/{customerId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse> getDetails(@PathVariable long customerId) throws ResourceNotfoundException {
        UserAccountDTO userAccountDTO = accountService.getUserInfo(customerId);
        return new ResponseEntity<>(ApiResponse.builder()
                .message("Account details loaded")
                .data(userAccountDTO)
                .status(true)
                .build(), HttpStatus.OK);
    }
}

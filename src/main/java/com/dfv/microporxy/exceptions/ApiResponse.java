package com.dfv.microporxy.exceptions;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiResponse {
    private boolean status;
    private String message;
    private Object data;
}

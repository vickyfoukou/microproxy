package com.dfv.microporxy.exceptions;

public class ResourceNotfoundException extends Exception {

    private final String message;

    public ResourceNotfoundException(String message){
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

package com.dfv.microporxy.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@ControllerAdvice
@Slf4j
public class ControllerErrorHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ApiResponse> handleGlobalException(Exception e){
        log.error(e.getMessage());
        return  new ResponseEntity<>(ApiResponse.builder()
                .status(false)
                .data(null)
                .message("Something went wrong. Please try again")
                .build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = { ResourceNotfoundException.class,})
    public ResponseEntity<ApiResponse> handleOtherException(ResourceNotfoundException e){
        log.error(e.getMessage());
        return  new ResponseEntity<>(ApiResponse.builder()
                .status(false)
                .data(null)
                .message(e.getMessage())
                .build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<ApiResponse> handleMethodArgumentNotValid(MethodArgumentNotValidException e) {
        List<String> errors = new ArrayList<>();
        e.getBindingResult().getFieldErrors().
                forEach(fieldError -> errors.add(fieldError.getField().concat(": ")
                        .concat(Objects.requireNonNull(fieldError.getDefaultMessage()))));

        String message = String.join(", ", errors);
        log.error(message);
        return  new ResponseEntity<>(ApiResponse.builder()
                .status(false)
                .data(null)
                .message(message)
                .build(), HttpStatus.BAD_REQUEST);
    }
}

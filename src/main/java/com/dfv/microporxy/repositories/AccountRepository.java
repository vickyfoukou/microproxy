package com.dfv.microporxy.repositories;

import com.dfv.microporxy.entities.Account;
import com.dfv.microporxy.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByCustomer(Customer customer);
}

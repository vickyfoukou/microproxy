package com.dfv.microporxy.repositories;

import com.dfv.microporxy.entities.Account;
import com.dfv.microporxy.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findAllByAccount(Account account);
}

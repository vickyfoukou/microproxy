package com.dfv.microporxy.repositories;

import com.dfv.microporxy.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}

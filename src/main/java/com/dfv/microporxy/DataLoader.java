package com.dfv.microporxy;

import com.dfv.microporxy.entities.Customer;
import com.dfv.microporxy.repositories.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DataLoader {
    private final CustomerRepository customerRepository;

    @Autowired
    public DataLoader(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
        loadCustomers();
    }

    private void loadCustomers(){
        customerRepository.save(Customer.builder().name("Djoulako").surname("Vicky").build());
        customerRepository.save(Customer.builder().name("Kunshans").surname("Jurg").build());
        log.info("default customers added into database;");
    }
}

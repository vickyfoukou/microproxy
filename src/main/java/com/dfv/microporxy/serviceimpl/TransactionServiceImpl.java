package com.dfv.microporxy.serviceimpl;

import com.dfv.microporxy.entities.Account;
import com.dfv.microporxy.entities.Transaction;
import com.dfv.microporxy.repositories.TransactionRepository;
import com.dfv.microporxy.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Transaction save(Transaction transaction) {
        return this.transactionRepository.save(transaction);
    }

    @Override
    public List<Transaction> findAllByAccount(Account account) {
        return transactionRepository.findAllByAccount(account);
    }
}

package com.dfv.microporxy.serviceimpl;

import com.dfv.microporxy.dto.OpenAccountDTO;
import com.dfv.microporxy.dto.TransactionDTO;
import com.dfv.microporxy.dto.UserAccountDTO;
import com.dfv.microporxy.entities.Account;
import com.dfv.microporxy.entities.Customer;
import com.dfv.microporxy.entities.Transaction;
import com.dfv.microporxy.exceptions.ResourceNotfoundException;
import com.dfv.microporxy.repositories.AccountRepository;
import com.dfv.microporxy.repositories.CustomerRepository;
import com.dfv.microporxy.services.AccountService;
import com.dfv.microporxy.services.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;
    private final TransactionService transactionService;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository,
                              CustomerRepository customerRepository, TransactionService transactionService) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
        this.transactionService = transactionService;
    }

    @Override
    @Transactional
    public void open_account(OpenAccountDTO openAccountDTO) throws ResourceNotfoundException {
        Customer customer = getCustomer(openAccountDTO.getCustomerId());

        Account account = Account.builder()
            .balance(openAccountDTO.getInitialCredit())
            .customer(customer).build();

        accountRepository.save(account);
        log.info("Account successfully created");

        if(openAccountDTO.getInitialCredit() > 0) {
            Transaction transaction = Transaction.builder()
                    .account(account)
                    .amount(openAccountDTO.getInitialCredit())
                    .build();

            transactionService.save(transaction);
            log.info("transaction details successfully created");
        }
    }

    @Override
    public UserAccountDTO getUserInfo(long customerId) throws ResourceNotfoundException {

        Customer customer = getCustomer(customerId);

        Account account = accountRepository.findByCustomer(customer).orElseThrow(
                () -> new ResourceNotfoundException("Account not exists for this customer")
        );

        List<TransactionDTO> transactions = transactionService.findAllByAccount(account).stream()
                .map(TransactionDTO::new).collect(Collectors.toList());

        UserAccountDTO userAccountDTO =  UserAccountDTO.builder()
                .name(customer.getName())
                .surname(customer.getSurname())
                .balance(account.getBalance())
                .transactions(transactions)
                .build();

        log.info("User account details : {}", userAccountDTO);

        return userAccountDTO;
    }

    public Customer getCustomer(long customerId) throws ResourceNotfoundException {
        return customerRepository.findById(customerId).orElseThrow(
                () -> new ResourceNotfoundException(String.format("customer not found  with  ID = %s", customerId))
        );
    }
}

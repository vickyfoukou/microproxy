package com.dfv.microporxy.services;

import com.dfv.microporxy.entities.Account;
import com.dfv.microporxy.entities.Transaction;

import java.util.List;

public interface TransactionService {
    Transaction save(Transaction transaction);
    List<Transaction> findAllByAccount(Account account);
}

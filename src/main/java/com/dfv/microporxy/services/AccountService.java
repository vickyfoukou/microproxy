package com.dfv.microporxy.services;

import com.dfv.microporxy.dto.OpenAccountDTO;
import com.dfv.microporxy.dto.UserAccountDTO;
import com.dfv.microporxy.entities.Customer;
import com.dfv.microporxy.exceptions.ResourceNotfoundException;

public interface AccountService {
    void open_account(OpenAccountDTO openAccountDTO) throws ResourceNotfoundException;
    UserAccountDTO getUserInfo(long customerId) throws ResourceNotfoundException;
    Customer getCustomer(long customerId) throws ResourceNotfoundException;
}
